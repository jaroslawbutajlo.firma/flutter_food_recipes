import 'package:dio/dio.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

Dio _createDioClient() {
  return Dio()
    ..options = BaseOptions(
      baseUrl: 'https://api.edamam.com/api/recipes/v2/',
      queryParameters: {
        'app_id': dotenv.env['APP_ID'],
        'app_key': dotenv.env['APP_KEY']
      },
      contentType: Headers.jsonContentType,
    )
    ..interceptors.add(PrettyDioLogger());
}

final dioProvider = Provider((_) => _createDioClient());
