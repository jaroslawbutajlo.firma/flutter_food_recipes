import 'package:dio/dio.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:food_recipes/data/dio_client.dart';
import 'package:food_recipes/data/recipes_api_models.dart';
import 'package:retrofit/retrofit.dart';

part 'recipes_api.g.dart';

@RestApi()
abstract class RecipesApi {
  factory RecipesApi(Dio dio) = _RecipesApi;

  @GET('/')
  Future<RecipesList> getRecipesList(
    @Query('q') String query, {
    @Query('type') String type = 'public',
  });
}

final recipesApiProvider = Provider((ref) => RecipesApi(ref.read(dioProvider)));
