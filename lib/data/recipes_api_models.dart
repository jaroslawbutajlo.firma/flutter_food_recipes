import 'package:freezed_annotation/freezed_annotation.dart';

part 'recipes_api_models.freezed.dart';
part 'recipes_api_models.g.dart';

@freezed
class RecipesList with _$RecipesList {
  const factory RecipesList({
    int? from,
    int? to,
    int? count,
    List<Hit>? hits,
  }) = _RecipesList;

  factory RecipesList.fromJson(Map<String, dynamic> json) =>
      _$RecipesListFromJson(json);
}

@freezed
class Hit with _$Hit {
  const factory Hit({
    Recipe? recipe,
  }) = _Hit;

  factory Hit.fromJson(Map<String, dynamic> json) => _$HitFromJson(json);
}

@freezed
class Recipe with _$Recipe {
  const factory Recipe({
    String? uri,
    String? label,
    String? image,
    String? source,
    String? url,
    String? shareAs,
    int? recipeYield,
    List<String>? dietLabels,
    List<String>? healthLabels,
    List<String>? cautions,
    List<String>? ingredientLines,
    List<Ingredient>? ingredients,
    double? calories,
    double? totalWeight,
    List<String>? cuisineType,
    List<String>? mealType,
    List<String>? dishType,
  }) = _Recipe;

  factory Recipe.fromJson(Map<String, dynamic> json) => _$RecipeFromJson(json);
}

@freezed
class Ingredient with _$Ingredient {
  const factory Ingredient({
    String? text,
    int? quantity,
    String? measure,
    String? food,
    double? weight,
    String? foodId,
  }) = _Ingredient;

  factory Ingredient.fromJson(Map<String, dynamic> json) =>
      _$IngredientFromJson(json);
}
