// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'recipes_api_models.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

RecipesList _$RecipesListFromJson(Map<String, dynamic> json) {
  return _RecipesList.fromJson(json);
}

/// @nodoc
class _$RecipesListTearOff {
  const _$RecipesListTearOff();

  _RecipesList call({int? from, int? to, int? count, List<Hit>? hits}) {
    return _RecipesList(
      from: from,
      to: to,
      count: count,
      hits: hits,
    );
  }

  RecipesList fromJson(Map<String, Object> json) {
    return RecipesList.fromJson(json);
  }
}

/// @nodoc
const $RecipesList = _$RecipesListTearOff();

/// @nodoc
mixin _$RecipesList {
  int? get from => throw _privateConstructorUsedError;
  int? get to => throw _privateConstructorUsedError;
  int? get count => throw _privateConstructorUsedError;
  List<Hit>? get hits => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $RecipesListCopyWith<RecipesList> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $RecipesListCopyWith<$Res> {
  factory $RecipesListCopyWith(
          RecipesList value, $Res Function(RecipesList) then) =
      _$RecipesListCopyWithImpl<$Res>;
  $Res call({int? from, int? to, int? count, List<Hit>? hits});
}

/// @nodoc
class _$RecipesListCopyWithImpl<$Res> implements $RecipesListCopyWith<$Res> {
  _$RecipesListCopyWithImpl(this._value, this._then);

  final RecipesList _value;
  // ignore: unused_field
  final $Res Function(RecipesList) _then;

  @override
  $Res call({
    Object? from = freezed,
    Object? to = freezed,
    Object? count = freezed,
    Object? hits = freezed,
  }) {
    return _then(_value.copyWith(
      from: from == freezed
          ? _value.from
          : from // ignore: cast_nullable_to_non_nullable
              as int?,
      to: to == freezed
          ? _value.to
          : to // ignore: cast_nullable_to_non_nullable
              as int?,
      count: count == freezed
          ? _value.count
          : count // ignore: cast_nullable_to_non_nullable
              as int?,
      hits: hits == freezed
          ? _value.hits
          : hits // ignore: cast_nullable_to_non_nullable
              as List<Hit>?,
    ));
  }
}

/// @nodoc
abstract class _$RecipesListCopyWith<$Res>
    implements $RecipesListCopyWith<$Res> {
  factory _$RecipesListCopyWith(
          _RecipesList value, $Res Function(_RecipesList) then) =
      __$RecipesListCopyWithImpl<$Res>;
  @override
  $Res call({int? from, int? to, int? count, List<Hit>? hits});
}

/// @nodoc
class __$RecipesListCopyWithImpl<$Res> extends _$RecipesListCopyWithImpl<$Res>
    implements _$RecipesListCopyWith<$Res> {
  __$RecipesListCopyWithImpl(
      _RecipesList _value, $Res Function(_RecipesList) _then)
      : super(_value, (v) => _then(v as _RecipesList));

  @override
  _RecipesList get _value => super._value as _RecipesList;

  @override
  $Res call({
    Object? from = freezed,
    Object? to = freezed,
    Object? count = freezed,
    Object? hits = freezed,
  }) {
    return _then(_RecipesList(
      from: from == freezed
          ? _value.from
          : from // ignore: cast_nullable_to_non_nullable
              as int?,
      to: to == freezed
          ? _value.to
          : to // ignore: cast_nullable_to_non_nullable
              as int?,
      count: count == freezed
          ? _value.count
          : count // ignore: cast_nullable_to_non_nullable
              as int?,
      hits: hits == freezed
          ? _value.hits
          : hits // ignore: cast_nullable_to_non_nullable
              as List<Hit>?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_RecipesList implements _RecipesList {
  const _$_RecipesList({this.from, this.to, this.count, this.hits});

  factory _$_RecipesList.fromJson(Map<String, dynamic> json) =>
      _$_$_RecipesListFromJson(json);

  @override
  final int? from;
  @override
  final int? to;
  @override
  final int? count;
  @override
  final List<Hit>? hits;

  @override
  String toString() {
    return 'RecipesList(from: $from, to: $to, count: $count, hits: $hits)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _RecipesList &&
            (identical(other.from, from) ||
                const DeepCollectionEquality().equals(other.from, from)) &&
            (identical(other.to, to) ||
                const DeepCollectionEquality().equals(other.to, to)) &&
            (identical(other.count, count) ||
                const DeepCollectionEquality().equals(other.count, count)) &&
            (identical(other.hits, hits) ||
                const DeepCollectionEquality().equals(other.hits, hits)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(from) ^
      const DeepCollectionEquality().hash(to) ^
      const DeepCollectionEquality().hash(count) ^
      const DeepCollectionEquality().hash(hits);

  @JsonKey(ignore: true)
  @override
  _$RecipesListCopyWith<_RecipesList> get copyWith =>
      __$RecipesListCopyWithImpl<_RecipesList>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_RecipesListToJson(this);
  }
}

abstract class _RecipesList implements RecipesList {
  const factory _RecipesList(
      {int? from, int? to, int? count, List<Hit>? hits}) = _$_RecipesList;

  factory _RecipesList.fromJson(Map<String, dynamic> json) =
      _$_RecipesList.fromJson;

  @override
  int? get from => throw _privateConstructorUsedError;
  @override
  int? get to => throw _privateConstructorUsedError;
  @override
  int? get count => throw _privateConstructorUsedError;
  @override
  List<Hit>? get hits => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$RecipesListCopyWith<_RecipesList> get copyWith =>
      throw _privateConstructorUsedError;
}

Hit _$HitFromJson(Map<String, dynamic> json) {
  return _Hit.fromJson(json);
}

/// @nodoc
class _$HitTearOff {
  const _$HitTearOff();

  _Hit call({Recipe? recipe}) {
    return _Hit(
      recipe: recipe,
    );
  }

  Hit fromJson(Map<String, Object> json) {
    return Hit.fromJson(json);
  }
}

/// @nodoc
const $Hit = _$HitTearOff();

/// @nodoc
mixin _$Hit {
  Recipe? get recipe => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $HitCopyWith<Hit> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $HitCopyWith<$Res> {
  factory $HitCopyWith(Hit value, $Res Function(Hit) then) =
      _$HitCopyWithImpl<$Res>;
  $Res call({Recipe? recipe});

  $RecipeCopyWith<$Res>? get recipe;
}

/// @nodoc
class _$HitCopyWithImpl<$Res> implements $HitCopyWith<$Res> {
  _$HitCopyWithImpl(this._value, this._then);

  final Hit _value;
  // ignore: unused_field
  final $Res Function(Hit) _then;

  @override
  $Res call({
    Object? recipe = freezed,
  }) {
    return _then(_value.copyWith(
      recipe: recipe == freezed
          ? _value.recipe
          : recipe // ignore: cast_nullable_to_non_nullable
              as Recipe?,
    ));
  }

  @override
  $RecipeCopyWith<$Res>? get recipe {
    if (_value.recipe == null) {
      return null;
    }

    return $RecipeCopyWith<$Res>(_value.recipe!, (value) {
      return _then(_value.copyWith(recipe: value));
    });
  }
}

/// @nodoc
abstract class _$HitCopyWith<$Res> implements $HitCopyWith<$Res> {
  factory _$HitCopyWith(_Hit value, $Res Function(_Hit) then) =
      __$HitCopyWithImpl<$Res>;
  @override
  $Res call({Recipe? recipe});

  @override
  $RecipeCopyWith<$Res>? get recipe;
}

/// @nodoc
class __$HitCopyWithImpl<$Res> extends _$HitCopyWithImpl<$Res>
    implements _$HitCopyWith<$Res> {
  __$HitCopyWithImpl(_Hit _value, $Res Function(_Hit) _then)
      : super(_value, (v) => _then(v as _Hit));

  @override
  _Hit get _value => super._value as _Hit;

  @override
  $Res call({
    Object? recipe = freezed,
  }) {
    return _then(_Hit(
      recipe: recipe == freezed
          ? _value.recipe
          : recipe // ignore: cast_nullable_to_non_nullable
              as Recipe?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Hit implements _Hit {
  const _$_Hit({this.recipe});

  factory _$_Hit.fromJson(Map<String, dynamic> json) => _$_$_HitFromJson(json);

  @override
  final Recipe? recipe;

  @override
  String toString() {
    return 'Hit(recipe: $recipe)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Hit &&
            (identical(other.recipe, recipe) ||
                const DeepCollectionEquality().equals(other.recipe, recipe)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(recipe);

  @JsonKey(ignore: true)
  @override
  _$HitCopyWith<_Hit> get copyWith =>
      __$HitCopyWithImpl<_Hit>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_HitToJson(this);
  }
}

abstract class _Hit implements Hit {
  const factory _Hit({Recipe? recipe}) = _$_Hit;

  factory _Hit.fromJson(Map<String, dynamic> json) = _$_Hit.fromJson;

  @override
  Recipe? get recipe => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$HitCopyWith<_Hit> get copyWith => throw _privateConstructorUsedError;
}

Recipe _$RecipeFromJson(Map<String, dynamic> json) {
  return _Recipe.fromJson(json);
}

/// @nodoc
class _$RecipeTearOff {
  const _$RecipeTearOff();

  _Recipe call(
      {String? uri,
      String? label,
      String? image,
      String? source,
      String? url,
      String? shareAs,
      int? recipeYield,
      List<String>? dietLabels,
      List<String>? healthLabels,
      List<String>? cautions,
      List<String>? ingredientLines,
      List<Ingredient>? ingredients,
      double? calories,
      double? totalWeight,
      List<String>? cuisineType,
      List<String>? mealType,
      List<String>? dishType}) {
    return _Recipe(
      uri: uri,
      label: label,
      image: image,
      source: source,
      url: url,
      shareAs: shareAs,
      recipeYield: recipeYield,
      dietLabels: dietLabels,
      healthLabels: healthLabels,
      cautions: cautions,
      ingredientLines: ingredientLines,
      ingredients: ingredients,
      calories: calories,
      totalWeight: totalWeight,
      cuisineType: cuisineType,
      mealType: mealType,
      dishType: dishType,
    );
  }

  Recipe fromJson(Map<String, Object> json) {
    return Recipe.fromJson(json);
  }
}

/// @nodoc
const $Recipe = _$RecipeTearOff();

/// @nodoc
mixin _$Recipe {
  String? get uri => throw _privateConstructorUsedError;
  String? get label => throw _privateConstructorUsedError;
  String? get image => throw _privateConstructorUsedError;
  String? get source => throw _privateConstructorUsedError;
  String? get url => throw _privateConstructorUsedError;
  String? get shareAs => throw _privateConstructorUsedError;
  int? get recipeYield => throw _privateConstructorUsedError;
  List<String>? get dietLabels => throw _privateConstructorUsedError;
  List<String>? get healthLabels => throw _privateConstructorUsedError;
  List<String>? get cautions => throw _privateConstructorUsedError;
  List<String>? get ingredientLines => throw _privateConstructorUsedError;
  List<Ingredient>? get ingredients => throw _privateConstructorUsedError;
  double? get calories => throw _privateConstructorUsedError;
  double? get totalWeight => throw _privateConstructorUsedError;
  List<String>? get cuisineType => throw _privateConstructorUsedError;
  List<String>? get mealType => throw _privateConstructorUsedError;
  List<String>? get dishType => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $RecipeCopyWith<Recipe> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $RecipeCopyWith<$Res> {
  factory $RecipeCopyWith(Recipe value, $Res Function(Recipe) then) =
      _$RecipeCopyWithImpl<$Res>;
  $Res call(
      {String? uri,
      String? label,
      String? image,
      String? source,
      String? url,
      String? shareAs,
      int? recipeYield,
      List<String>? dietLabels,
      List<String>? healthLabels,
      List<String>? cautions,
      List<String>? ingredientLines,
      List<Ingredient>? ingredients,
      double? calories,
      double? totalWeight,
      List<String>? cuisineType,
      List<String>? mealType,
      List<String>? dishType});
}

/// @nodoc
class _$RecipeCopyWithImpl<$Res> implements $RecipeCopyWith<$Res> {
  _$RecipeCopyWithImpl(this._value, this._then);

  final Recipe _value;
  // ignore: unused_field
  final $Res Function(Recipe) _then;

  @override
  $Res call({
    Object? uri = freezed,
    Object? label = freezed,
    Object? image = freezed,
    Object? source = freezed,
    Object? url = freezed,
    Object? shareAs = freezed,
    Object? recipeYield = freezed,
    Object? dietLabels = freezed,
    Object? healthLabels = freezed,
    Object? cautions = freezed,
    Object? ingredientLines = freezed,
    Object? ingredients = freezed,
    Object? calories = freezed,
    Object? totalWeight = freezed,
    Object? cuisineType = freezed,
    Object? mealType = freezed,
    Object? dishType = freezed,
  }) {
    return _then(_value.copyWith(
      uri: uri == freezed
          ? _value.uri
          : uri // ignore: cast_nullable_to_non_nullable
              as String?,
      label: label == freezed
          ? _value.label
          : label // ignore: cast_nullable_to_non_nullable
              as String?,
      image: image == freezed
          ? _value.image
          : image // ignore: cast_nullable_to_non_nullable
              as String?,
      source: source == freezed
          ? _value.source
          : source // ignore: cast_nullable_to_non_nullable
              as String?,
      url: url == freezed
          ? _value.url
          : url // ignore: cast_nullable_to_non_nullable
              as String?,
      shareAs: shareAs == freezed
          ? _value.shareAs
          : shareAs // ignore: cast_nullable_to_non_nullable
              as String?,
      recipeYield: recipeYield == freezed
          ? _value.recipeYield
          : recipeYield // ignore: cast_nullable_to_non_nullable
              as int?,
      dietLabels: dietLabels == freezed
          ? _value.dietLabels
          : dietLabels // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      healthLabels: healthLabels == freezed
          ? _value.healthLabels
          : healthLabels // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      cautions: cautions == freezed
          ? _value.cautions
          : cautions // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      ingredientLines: ingredientLines == freezed
          ? _value.ingredientLines
          : ingredientLines // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      ingredients: ingredients == freezed
          ? _value.ingredients
          : ingredients // ignore: cast_nullable_to_non_nullable
              as List<Ingredient>?,
      calories: calories == freezed
          ? _value.calories
          : calories // ignore: cast_nullable_to_non_nullable
              as double?,
      totalWeight: totalWeight == freezed
          ? _value.totalWeight
          : totalWeight // ignore: cast_nullable_to_non_nullable
              as double?,
      cuisineType: cuisineType == freezed
          ? _value.cuisineType
          : cuisineType // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      mealType: mealType == freezed
          ? _value.mealType
          : mealType // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      dishType: dishType == freezed
          ? _value.dishType
          : dishType // ignore: cast_nullable_to_non_nullable
              as List<String>?,
    ));
  }
}

/// @nodoc
abstract class _$RecipeCopyWith<$Res> implements $RecipeCopyWith<$Res> {
  factory _$RecipeCopyWith(_Recipe value, $Res Function(_Recipe) then) =
      __$RecipeCopyWithImpl<$Res>;
  @override
  $Res call(
      {String? uri,
      String? label,
      String? image,
      String? source,
      String? url,
      String? shareAs,
      int? recipeYield,
      List<String>? dietLabels,
      List<String>? healthLabels,
      List<String>? cautions,
      List<String>? ingredientLines,
      List<Ingredient>? ingredients,
      double? calories,
      double? totalWeight,
      List<String>? cuisineType,
      List<String>? mealType,
      List<String>? dishType});
}

/// @nodoc
class __$RecipeCopyWithImpl<$Res> extends _$RecipeCopyWithImpl<$Res>
    implements _$RecipeCopyWith<$Res> {
  __$RecipeCopyWithImpl(_Recipe _value, $Res Function(_Recipe) _then)
      : super(_value, (v) => _then(v as _Recipe));

  @override
  _Recipe get _value => super._value as _Recipe;

  @override
  $Res call({
    Object? uri = freezed,
    Object? label = freezed,
    Object? image = freezed,
    Object? source = freezed,
    Object? url = freezed,
    Object? shareAs = freezed,
    Object? recipeYield = freezed,
    Object? dietLabels = freezed,
    Object? healthLabels = freezed,
    Object? cautions = freezed,
    Object? ingredientLines = freezed,
    Object? ingredients = freezed,
    Object? calories = freezed,
    Object? totalWeight = freezed,
    Object? cuisineType = freezed,
    Object? mealType = freezed,
    Object? dishType = freezed,
  }) {
    return _then(_Recipe(
      uri: uri == freezed
          ? _value.uri
          : uri // ignore: cast_nullable_to_non_nullable
              as String?,
      label: label == freezed
          ? _value.label
          : label // ignore: cast_nullable_to_non_nullable
              as String?,
      image: image == freezed
          ? _value.image
          : image // ignore: cast_nullable_to_non_nullable
              as String?,
      source: source == freezed
          ? _value.source
          : source // ignore: cast_nullable_to_non_nullable
              as String?,
      url: url == freezed
          ? _value.url
          : url // ignore: cast_nullable_to_non_nullable
              as String?,
      shareAs: shareAs == freezed
          ? _value.shareAs
          : shareAs // ignore: cast_nullable_to_non_nullable
              as String?,
      recipeYield: recipeYield == freezed
          ? _value.recipeYield
          : recipeYield // ignore: cast_nullable_to_non_nullable
              as int?,
      dietLabels: dietLabels == freezed
          ? _value.dietLabels
          : dietLabels // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      healthLabels: healthLabels == freezed
          ? _value.healthLabels
          : healthLabels // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      cautions: cautions == freezed
          ? _value.cautions
          : cautions // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      ingredientLines: ingredientLines == freezed
          ? _value.ingredientLines
          : ingredientLines // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      ingredients: ingredients == freezed
          ? _value.ingredients
          : ingredients // ignore: cast_nullable_to_non_nullable
              as List<Ingredient>?,
      calories: calories == freezed
          ? _value.calories
          : calories // ignore: cast_nullable_to_non_nullable
              as double?,
      totalWeight: totalWeight == freezed
          ? _value.totalWeight
          : totalWeight // ignore: cast_nullable_to_non_nullable
              as double?,
      cuisineType: cuisineType == freezed
          ? _value.cuisineType
          : cuisineType // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      mealType: mealType == freezed
          ? _value.mealType
          : mealType // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      dishType: dishType == freezed
          ? _value.dishType
          : dishType // ignore: cast_nullable_to_non_nullable
              as List<String>?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Recipe implements _Recipe {
  const _$_Recipe(
      {this.uri,
      this.label,
      this.image,
      this.source,
      this.url,
      this.shareAs,
      this.recipeYield,
      this.dietLabels,
      this.healthLabels,
      this.cautions,
      this.ingredientLines,
      this.ingredients,
      this.calories,
      this.totalWeight,
      this.cuisineType,
      this.mealType,
      this.dishType});

  factory _$_Recipe.fromJson(Map<String, dynamic> json) =>
      _$_$_RecipeFromJson(json);

  @override
  final String? uri;
  @override
  final String? label;
  @override
  final String? image;
  @override
  final String? source;
  @override
  final String? url;
  @override
  final String? shareAs;
  @override
  final int? recipeYield;
  @override
  final List<String>? dietLabels;
  @override
  final List<String>? healthLabels;
  @override
  final List<String>? cautions;
  @override
  final List<String>? ingredientLines;
  @override
  final List<Ingredient>? ingredients;
  @override
  final double? calories;
  @override
  final double? totalWeight;
  @override
  final List<String>? cuisineType;
  @override
  final List<String>? mealType;
  @override
  final List<String>? dishType;

  @override
  String toString() {
    return 'Recipe(uri: $uri, label: $label, image: $image, source: $source, url: $url, shareAs: $shareAs, recipeYield: $recipeYield, dietLabels: $dietLabels, healthLabels: $healthLabels, cautions: $cautions, ingredientLines: $ingredientLines, ingredients: $ingredients, calories: $calories, totalWeight: $totalWeight, cuisineType: $cuisineType, mealType: $mealType, dishType: $dishType)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Recipe &&
            (identical(other.uri, uri) ||
                const DeepCollectionEquality().equals(other.uri, uri)) &&
            (identical(other.label, label) ||
                const DeepCollectionEquality().equals(other.label, label)) &&
            (identical(other.image, image) ||
                const DeepCollectionEquality().equals(other.image, image)) &&
            (identical(other.source, source) ||
                const DeepCollectionEquality().equals(other.source, source)) &&
            (identical(other.url, url) ||
                const DeepCollectionEquality().equals(other.url, url)) &&
            (identical(other.shareAs, shareAs) ||
                const DeepCollectionEquality()
                    .equals(other.shareAs, shareAs)) &&
            (identical(other.recipeYield, recipeYield) ||
                const DeepCollectionEquality()
                    .equals(other.recipeYield, recipeYield)) &&
            (identical(other.dietLabels, dietLabels) ||
                const DeepCollectionEquality()
                    .equals(other.dietLabels, dietLabels)) &&
            (identical(other.healthLabels, healthLabels) ||
                const DeepCollectionEquality()
                    .equals(other.healthLabels, healthLabels)) &&
            (identical(other.cautions, cautions) ||
                const DeepCollectionEquality()
                    .equals(other.cautions, cautions)) &&
            (identical(other.ingredientLines, ingredientLines) ||
                const DeepCollectionEquality()
                    .equals(other.ingredientLines, ingredientLines)) &&
            (identical(other.ingredients, ingredients) ||
                const DeepCollectionEquality()
                    .equals(other.ingredients, ingredients)) &&
            (identical(other.calories, calories) ||
                const DeepCollectionEquality()
                    .equals(other.calories, calories)) &&
            (identical(other.totalWeight, totalWeight) ||
                const DeepCollectionEquality()
                    .equals(other.totalWeight, totalWeight)) &&
            (identical(other.cuisineType, cuisineType) ||
                const DeepCollectionEquality()
                    .equals(other.cuisineType, cuisineType)) &&
            (identical(other.mealType, mealType) ||
                const DeepCollectionEquality()
                    .equals(other.mealType, mealType)) &&
            (identical(other.dishType, dishType) ||
                const DeepCollectionEquality()
                    .equals(other.dishType, dishType)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(uri) ^
      const DeepCollectionEquality().hash(label) ^
      const DeepCollectionEquality().hash(image) ^
      const DeepCollectionEquality().hash(source) ^
      const DeepCollectionEquality().hash(url) ^
      const DeepCollectionEquality().hash(shareAs) ^
      const DeepCollectionEquality().hash(recipeYield) ^
      const DeepCollectionEquality().hash(dietLabels) ^
      const DeepCollectionEquality().hash(healthLabels) ^
      const DeepCollectionEquality().hash(cautions) ^
      const DeepCollectionEquality().hash(ingredientLines) ^
      const DeepCollectionEquality().hash(ingredients) ^
      const DeepCollectionEquality().hash(calories) ^
      const DeepCollectionEquality().hash(totalWeight) ^
      const DeepCollectionEquality().hash(cuisineType) ^
      const DeepCollectionEquality().hash(mealType) ^
      const DeepCollectionEquality().hash(dishType);

  @JsonKey(ignore: true)
  @override
  _$RecipeCopyWith<_Recipe> get copyWith =>
      __$RecipeCopyWithImpl<_Recipe>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_RecipeToJson(this);
  }
}

abstract class _Recipe implements Recipe {
  const factory _Recipe(
      {String? uri,
      String? label,
      String? image,
      String? source,
      String? url,
      String? shareAs,
      int? recipeYield,
      List<String>? dietLabels,
      List<String>? healthLabels,
      List<String>? cautions,
      List<String>? ingredientLines,
      List<Ingredient>? ingredients,
      double? calories,
      double? totalWeight,
      List<String>? cuisineType,
      List<String>? mealType,
      List<String>? dishType}) = _$_Recipe;

  factory _Recipe.fromJson(Map<String, dynamic> json) = _$_Recipe.fromJson;

  @override
  String? get uri => throw _privateConstructorUsedError;
  @override
  String? get label => throw _privateConstructorUsedError;
  @override
  String? get image => throw _privateConstructorUsedError;
  @override
  String? get source => throw _privateConstructorUsedError;
  @override
  String? get url => throw _privateConstructorUsedError;
  @override
  String? get shareAs => throw _privateConstructorUsedError;
  @override
  int? get recipeYield => throw _privateConstructorUsedError;
  @override
  List<String>? get dietLabels => throw _privateConstructorUsedError;
  @override
  List<String>? get healthLabels => throw _privateConstructorUsedError;
  @override
  List<String>? get cautions => throw _privateConstructorUsedError;
  @override
  List<String>? get ingredientLines => throw _privateConstructorUsedError;
  @override
  List<Ingredient>? get ingredients => throw _privateConstructorUsedError;
  @override
  double? get calories => throw _privateConstructorUsedError;
  @override
  double? get totalWeight => throw _privateConstructorUsedError;
  @override
  List<String>? get cuisineType => throw _privateConstructorUsedError;
  @override
  List<String>? get mealType => throw _privateConstructorUsedError;
  @override
  List<String>? get dishType => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$RecipeCopyWith<_Recipe> get copyWith => throw _privateConstructorUsedError;
}

Ingredient _$IngredientFromJson(Map<String, dynamic> json) {
  return _Ingredient.fromJson(json);
}

/// @nodoc
class _$IngredientTearOff {
  const _$IngredientTearOff();

  _Ingredient call(
      {String? text,
      int? quantity,
      String? measure,
      String? food,
      double? weight,
      String? foodId}) {
    return _Ingredient(
      text: text,
      quantity: quantity,
      measure: measure,
      food: food,
      weight: weight,
      foodId: foodId,
    );
  }

  Ingredient fromJson(Map<String, Object> json) {
    return Ingredient.fromJson(json);
  }
}

/// @nodoc
const $Ingredient = _$IngredientTearOff();

/// @nodoc
mixin _$Ingredient {
  String? get text => throw _privateConstructorUsedError;
  int? get quantity => throw _privateConstructorUsedError;
  String? get measure => throw _privateConstructorUsedError;
  String? get food => throw _privateConstructorUsedError;
  double? get weight => throw _privateConstructorUsedError;
  String? get foodId => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $IngredientCopyWith<Ingredient> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $IngredientCopyWith<$Res> {
  factory $IngredientCopyWith(
          Ingredient value, $Res Function(Ingredient) then) =
      _$IngredientCopyWithImpl<$Res>;
  $Res call(
      {String? text,
      int? quantity,
      String? measure,
      String? food,
      double? weight,
      String? foodId});
}

/// @nodoc
class _$IngredientCopyWithImpl<$Res> implements $IngredientCopyWith<$Res> {
  _$IngredientCopyWithImpl(this._value, this._then);

  final Ingredient _value;
  // ignore: unused_field
  final $Res Function(Ingredient) _then;

  @override
  $Res call({
    Object? text = freezed,
    Object? quantity = freezed,
    Object? measure = freezed,
    Object? food = freezed,
    Object? weight = freezed,
    Object? foodId = freezed,
  }) {
    return _then(_value.copyWith(
      text: text == freezed
          ? _value.text
          : text // ignore: cast_nullable_to_non_nullable
              as String?,
      quantity: quantity == freezed
          ? _value.quantity
          : quantity // ignore: cast_nullable_to_non_nullable
              as int?,
      measure: measure == freezed
          ? _value.measure
          : measure // ignore: cast_nullable_to_non_nullable
              as String?,
      food: food == freezed
          ? _value.food
          : food // ignore: cast_nullable_to_non_nullable
              as String?,
      weight: weight == freezed
          ? _value.weight
          : weight // ignore: cast_nullable_to_non_nullable
              as double?,
      foodId: foodId == freezed
          ? _value.foodId
          : foodId // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
abstract class _$IngredientCopyWith<$Res> implements $IngredientCopyWith<$Res> {
  factory _$IngredientCopyWith(
          _Ingredient value, $Res Function(_Ingredient) then) =
      __$IngredientCopyWithImpl<$Res>;
  @override
  $Res call(
      {String? text,
      int? quantity,
      String? measure,
      String? food,
      double? weight,
      String? foodId});
}

/// @nodoc
class __$IngredientCopyWithImpl<$Res> extends _$IngredientCopyWithImpl<$Res>
    implements _$IngredientCopyWith<$Res> {
  __$IngredientCopyWithImpl(
      _Ingredient _value, $Res Function(_Ingredient) _then)
      : super(_value, (v) => _then(v as _Ingredient));

  @override
  _Ingredient get _value => super._value as _Ingredient;

  @override
  $Res call({
    Object? text = freezed,
    Object? quantity = freezed,
    Object? measure = freezed,
    Object? food = freezed,
    Object? weight = freezed,
    Object? foodId = freezed,
  }) {
    return _then(_Ingredient(
      text: text == freezed
          ? _value.text
          : text // ignore: cast_nullable_to_non_nullable
              as String?,
      quantity: quantity == freezed
          ? _value.quantity
          : quantity // ignore: cast_nullable_to_non_nullable
              as int?,
      measure: measure == freezed
          ? _value.measure
          : measure // ignore: cast_nullable_to_non_nullable
              as String?,
      food: food == freezed
          ? _value.food
          : food // ignore: cast_nullable_to_non_nullable
              as String?,
      weight: weight == freezed
          ? _value.weight
          : weight // ignore: cast_nullable_to_non_nullable
              as double?,
      foodId: foodId == freezed
          ? _value.foodId
          : foodId // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Ingredient implements _Ingredient {
  const _$_Ingredient(
      {this.text,
      this.quantity,
      this.measure,
      this.food,
      this.weight,
      this.foodId});

  factory _$_Ingredient.fromJson(Map<String, dynamic> json) =>
      _$_$_IngredientFromJson(json);

  @override
  final String? text;
  @override
  final int? quantity;
  @override
  final String? measure;
  @override
  final String? food;
  @override
  final double? weight;
  @override
  final String? foodId;

  @override
  String toString() {
    return 'Ingredient(text: $text, quantity: $quantity, measure: $measure, food: $food, weight: $weight, foodId: $foodId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Ingredient &&
            (identical(other.text, text) ||
                const DeepCollectionEquality().equals(other.text, text)) &&
            (identical(other.quantity, quantity) ||
                const DeepCollectionEquality()
                    .equals(other.quantity, quantity)) &&
            (identical(other.measure, measure) ||
                const DeepCollectionEquality()
                    .equals(other.measure, measure)) &&
            (identical(other.food, food) ||
                const DeepCollectionEquality().equals(other.food, food)) &&
            (identical(other.weight, weight) ||
                const DeepCollectionEquality().equals(other.weight, weight)) &&
            (identical(other.foodId, foodId) ||
                const DeepCollectionEquality().equals(other.foodId, foodId)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(text) ^
      const DeepCollectionEquality().hash(quantity) ^
      const DeepCollectionEquality().hash(measure) ^
      const DeepCollectionEquality().hash(food) ^
      const DeepCollectionEquality().hash(weight) ^
      const DeepCollectionEquality().hash(foodId);

  @JsonKey(ignore: true)
  @override
  _$IngredientCopyWith<_Ingredient> get copyWith =>
      __$IngredientCopyWithImpl<_Ingredient>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_IngredientToJson(this);
  }
}

abstract class _Ingredient implements Ingredient {
  const factory _Ingredient(
      {String? text,
      int? quantity,
      String? measure,
      String? food,
      double? weight,
      String? foodId}) = _$_Ingredient;

  factory _Ingredient.fromJson(Map<String, dynamic> json) =
      _$_Ingredient.fromJson;

  @override
  String? get text => throw _privateConstructorUsedError;
  @override
  int? get quantity => throw _privateConstructorUsedError;
  @override
  String? get measure => throw _privateConstructorUsedError;
  @override
  String? get food => throw _privateConstructorUsedError;
  @override
  double? get weight => throw _privateConstructorUsedError;
  @override
  String? get foodId => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$IngredientCopyWith<_Ingredient> get copyWith =>
      throw _privateConstructorUsedError;
}
