// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'recipes_api_models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_RecipesList _$_$_RecipesListFromJson(Map<String, dynamic> json) {
  return _$_RecipesList(
    from: json['from'] as int?,
    to: json['to'] as int?,
    count: json['count'] as int?,
    hits: (json['hits'] as List<dynamic>?)
        ?.map((e) => Hit.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$_$_RecipesListToJson(_$_RecipesList instance) =>
    <String, dynamic>{
      'from': instance.from,
      'to': instance.to,
      'count': instance.count,
      'hits': instance.hits,
    };

_$_Hit _$_$_HitFromJson(Map<String, dynamic> json) {
  return _$_Hit(
    recipe: json['recipe'] == null
        ? null
        : Recipe.fromJson(json['recipe'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$_$_HitToJson(_$_Hit instance) => <String, dynamic>{
      'recipe': instance.recipe,
    };

_$_Recipe _$_$_RecipeFromJson(Map<String, dynamic> json) {
  return _$_Recipe(
    uri: json['uri'] as String?,
    label: json['label'] as String?,
    image: json['image'] as String?,
    source: json['source'] as String?,
    url: json['url'] as String?,
    shareAs: json['shareAs'] as String?,
    recipeYield: json['recipeYield'] as int?,
    dietLabels: (json['dietLabels'] as List<dynamic>?)
        ?.map((e) => e as String)
        .toList(),
    healthLabels: (json['healthLabels'] as List<dynamic>?)
        ?.map((e) => e as String)
        .toList(),
    cautions:
        (json['cautions'] as List<dynamic>?)?.map((e) => e as String).toList(),
    ingredientLines: (json['ingredientLines'] as List<dynamic>?)
        ?.map((e) => e as String)
        .toList(),
    ingredients: (json['ingredients'] as List<dynamic>?)
        ?.map((e) => Ingredient.fromJson(e as Map<String, dynamic>))
        .toList(),
    calories: (json['calories'] as num?)?.toDouble(),
    totalWeight: (json['totalWeight'] as num?)?.toDouble(),
    cuisineType: (json['cuisineType'] as List<dynamic>?)
        ?.map((e) => e as String)
        .toList(),
    mealType:
        (json['mealType'] as List<dynamic>?)?.map((e) => e as String).toList(),
    dishType:
        (json['dishType'] as List<dynamic>?)?.map((e) => e as String).toList(),
  );
}

Map<String, dynamic> _$_$_RecipeToJson(_$_Recipe instance) => <String, dynamic>{
      'uri': instance.uri,
      'label': instance.label,
      'image': instance.image,
      'source': instance.source,
      'url': instance.url,
      'shareAs': instance.shareAs,
      'recipeYield': instance.recipeYield,
      'dietLabels': instance.dietLabels,
      'healthLabels': instance.healthLabels,
      'cautions': instance.cautions,
      'ingredientLines': instance.ingredientLines,
      'ingredients': instance.ingredients,
      'calories': instance.calories,
      'totalWeight': instance.totalWeight,
      'cuisineType': instance.cuisineType,
      'mealType': instance.mealType,
      'dishType': instance.dishType,
    };

_$_Ingredient _$_$_IngredientFromJson(Map<String, dynamic> json) {
  return _$_Ingredient(
    text: json['text'] as String?,
    quantity: json['quantity'] as int?,
    measure: json['measure'] as String?,
    food: json['food'] as String?,
    weight: (json['weight'] as num?)?.toDouble(),
    foodId: json['foodId'] as String?,
  );
}

Map<String, dynamic> _$_$_IngredientToJson(_$_Ingredient instance) =>
    <String, dynamic>{
      'text': instance.text,
      'quantity': instance.quantity,
      'measure': instance.measure,
      'food': instance.food,
      'weight': instance.weight,
      'foodId': instance.foodId,
    };
