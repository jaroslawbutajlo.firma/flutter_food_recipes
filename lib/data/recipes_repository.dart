import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:food_recipes/data/recipes_api.dart';
import 'package:food_recipes/data/recipes_api_models.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'recipes_repository.freezed.dart';

class RecipesRepository {
  final RecipesApi _api;

  RecipesRepository(this._api);

  Future<List<RecipeModel>> getRecipes(String query) async {
    final recipesList = await _api.getRecipesList(query);

    return recipesList.hits
            ?.where((hit) => hit.recipe != null)
            .map((hit) => _mapRecipeDtoToRecipeModel(hit.recipe!))
            .toList() ??
        [];
  }

  RecipeModel _mapRecipeDtoToRecipeModel(Recipe recipe) {
    return RecipeModel(
      title: recipe.label ?? '',
      imageUrl: recipe.image,
      calories: recipe.calories?.ceil(),
      ingredients: recipe.ingredientLines ?? [],
      recipeUrl: recipe.url,
    );
  }
}

@freezed
class RecipeModel with _$RecipeModel {
  const factory RecipeModel({
    required String title,
    required String? imageUrl,
    required int? calories,
    required List<String> ingredients,
    required String? recipeUrl,
  }) = _RecipeModel;
}

final recipesRepositoryProvider =
    Provider((ref) => RecipesRepository(ref.read(recipesApiProvider)));
