// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'recipes_repository.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$RecipeModelTearOff {
  const _$RecipeModelTearOff();

  _RecipeModel call(
      {required String title,
      required String? imageUrl,
      required int? calories,
      required List<String> ingredients,
      required String? recipeUrl}) {
    return _RecipeModel(
      title: title,
      imageUrl: imageUrl,
      calories: calories,
      ingredients: ingredients,
      recipeUrl: recipeUrl,
    );
  }
}

/// @nodoc
const $RecipeModel = _$RecipeModelTearOff();

/// @nodoc
mixin _$RecipeModel {
  String get title => throw _privateConstructorUsedError;
  String? get imageUrl => throw _privateConstructorUsedError;
  int? get calories => throw _privateConstructorUsedError;
  List<String> get ingredients => throw _privateConstructorUsedError;
  String? get recipeUrl => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $RecipeModelCopyWith<RecipeModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $RecipeModelCopyWith<$Res> {
  factory $RecipeModelCopyWith(
          RecipeModel value, $Res Function(RecipeModel) then) =
      _$RecipeModelCopyWithImpl<$Res>;
  $Res call(
      {String title,
      String? imageUrl,
      int? calories,
      List<String> ingredients,
      String? recipeUrl});
}

/// @nodoc
class _$RecipeModelCopyWithImpl<$Res> implements $RecipeModelCopyWith<$Res> {
  _$RecipeModelCopyWithImpl(this._value, this._then);

  final RecipeModel _value;
  // ignore: unused_field
  final $Res Function(RecipeModel) _then;

  @override
  $Res call({
    Object? title = freezed,
    Object? imageUrl = freezed,
    Object? calories = freezed,
    Object? ingredients = freezed,
    Object? recipeUrl = freezed,
  }) {
    return _then(_value.copyWith(
      title: title == freezed
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      imageUrl: imageUrl == freezed
          ? _value.imageUrl
          : imageUrl // ignore: cast_nullable_to_non_nullable
              as String?,
      calories: calories == freezed
          ? _value.calories
          : calories // ignore: cast_nullable_to_non_nullable
              as int?,
      ingredients: ingredients == freezed
          ? _value.ingredients
          : ingredients // ignore: cast_nullable_to_non_nullable
              as List<String>,
      recipeUrl: recipeUrl == freezed
          ? _value.recipeUrl
          : recipeUrl // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
abstract class _$RecipeModelCopyWith<$Res>
    implements $RecipeModelCopyWith<$Res> {
  factory _$RecipeModelCopyWith(
          _RecipeModel value, $Res Function(_RecipeModel) then) =
      __$RecipeModelCopyWithImpl<$Res>;
  @override
  $Res call(
      {String title,
      String? imageUrl,
      int? calories,
      List<String> ingredients,
      String? recipeUrl});
}

/// @nodoc
class __$RecipeModelCopyWithImpl<$Res> extends _$RecipeModelCopyWithImpl<$Res>
    implements _$RecipeModelCopyWith<$Res> {
  __$RecipeModelCopyWithImpl(
      _RecipeModel _value, $Res Function(_RecipeModel) _then)
      : super(_value, (v) => _then(v as _RecipeModel));

  @override
  _RecipeModel get _value => super._value as _RecipeModel;

  @override
  $Res call({
    Object? title = freezed,
    Object? imageUrl = freezed,
    Object? calories = freezed,
    Object? ingredients = freezed,
    Object? recipeUrl = freezed,
  }) {
    return _then(_RecipeModel(
      title: title == freezed
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      imageUrl: imageUrl == freezed
          ? _value.imageUrl
          : imageUrl // ignore: cast_nullable_to_non_nullable
              as String?,
      calories: calories == freezed
          ? _value.calories
          : calories // ignore: cast_nullable_to_non_nullable
              as int?,
      ingredients: ingredients == freezed
          ? _value.ingredients
          : ingredients // ignore: cast_nullable_to_non_nullable
              as List<String>,
      recipeUrl: recipeUrl == freezed
          ? _value.recipeUrl
          : recipeUrl // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

class _$_RecipeModel implements _RecipeModel {
  const _$_RecipeModel(
      {required this.title,
      required this.imageUrl,
      required this.calories,
      required this.ingredients,
      required this.recipeUrl});

  @override
  final String title;
  @override
  final String? imageUrl;
  @override
  final int? calories;
  @override
  final List<String> ingredients;
  @override
  final String? recipeUrl;

  @override
  String toString() {
    return 'RecipeModel(title: $title, imageUrl: $imageUrl, calories: $calories, ingredients: $ingredients, recipeUrl: $recipeUrl)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _RecipeModel &&
            (identical(other.title, title) ||
                const DeepCollectionEquality().equals(other.title, title)) &&
            (identical(other.imageUrl, imageUrl) ||
                const DeepCollectionEquality()
                    .equals(other.imageUrl, imageUrl)) &&
            (identical(other.calories, calories) ||
                const DeepCollectionEquality()
                    .equals(other.calories, calories)) &&
            (identical(other.ingredients, ingredients) ||
                const DeepCollectionEquality()
                    .equals(other.ingredients, ingredients)) &&
            (identical(other.recipeUrl, recipeUrl) ||
                const DeepCollectionEquality()
                    .equals(other.recipeUrl, recipeUrl)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(title) ^
      const DeepCollectionEquality().hash(imageUrl) ^
      const DeepCollectionEquality().hash(calories) ^
      const DeepCollectionEquality().hash(ingredients) ^
      const DeepCollectionEquality().hash(recipeUrl);

  @JsonKey(ignore: true)
  @override
  _$RecipeModelCopyWith<_RecipeModel> get copyWith =>
      __$RecipeModelCopyWithImpl<_RecipeModel>(this, _$identity);
}

abstract class _RecipeModel implements RecipeModel {
  const factory _RecipeModel(
      {required String title,
      required String? imageUrl,
      required int? calories,
      required List<String> ingredients,
      required String? recipeUrl}) = _$_RecipeModel;

  @override
  String get title => throw _privateConstructorUsedError;
  @override
  String? get imageUrl => throw _privateConstructorUsedError;
  @override
  int? get calories => throw _privateConstructorUsedError;
  @override
  List<String> get ingredients => throw _privateConstructorUsedError;
  @override
  String? get recipeUrl => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$RecipeModelCopyWith<_RecipeModel> get copyWith =>
      throw _privateConstructorUsedError;
}
