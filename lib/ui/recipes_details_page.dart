import 'package:flutter/material.dart';
import 'package:food_recipes/data/recipes_repository.dart';
import 'package:url_launcher/url_launcher.dart';

class RecipesDetailsPage extends StatelessWidget {
  const RecipesDetailsPage({Key? key, required this.recipe}) : super(key: key);

  final RecipeModel recipe;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Expanded(
            child: CustomScrollView(
              slivers: [
                SliverAppBar(
                  expandedHeight: 200,
                  flexibleSpace: recipe.imageUrl != null
                      ? FlexibleSpaceBar(
                          title: Text(recipe.title),
                          background: Image.network(recipe.imageUrl!,
                              fit: BoxFit.cover),
                        )
                      : null,
                  pinned: true,
                ),
                SliverToBoxAdapter(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Text(
                          'Ingredients',
                          style: Theme.of(context).textTheme.headline5,
                        ),
                      ),
                      ListView.builder(
                        physics: NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: recipe.ingredients.length,
                        itemBuilder: (context, index) {
                          return ListTile(
                            leading: Container(
                              padding: EdgeInsets.all(8),
                              child: Text('${index + 1}'),
                              decoration: BoxDecoration(
                                color: Theme.of(context).primaryColorLight,
                                shape: BoxShape.circle,
                              ),
                            ),
                            title: Text(recipe.ingredients[index]),
                          );
                        },
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
          if (recipe.recipeUrl != null)
            SafeArea(
              top: false,
              minimum: EdgeInsets.only(bottom: 24),
              child: ElevatedButton(
                onPressed: () => launch(recipe.recipeUrl!),
                child: Text('Go to web page'),
              ),
            ),
        ],
      ),
    );
  }
}
