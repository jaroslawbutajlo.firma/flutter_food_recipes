import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:food_recipes/ui/recipes_details_page.dart';
import 'package:food_recipes/ui/recipes_list_state.dart';

class RecipesListPage extends ConsumerWidget {
  const RecipesListPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    final searching = watch(searchQueryProvider).state.isNotEmpty;

    return Scaffold(
      appBar: AppBar(
        title: Text('Recipes List'),
        actions: [
          if (searching)
            IconButton(
              onPressed: () {
                context.read(searchQueryProvider).state = '';
              },
              icon: Icon(Icons.search),
            )
        ],
      ),
      body: searching ? _RecipesListBody() : _EmptySearchQueryBody(),
    );
  }
}

class _EmptySearchQueryBody extends StatefulWidget {
  const _EmptySearchQueryBody({Key? key}) : super(key: key);

  @override
  __EmptySearchQueryBodyState createState() => __EmptySearchQueryBodyState();
}

class __EmptySearchQueryBodyState extends State<_EmptySearchQueryBody> {
  String _searchQuery = '';

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(24),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            TextField(
              onChanged: (query) => _searchQuery = query,
              decoration: InputDecoration(labelText: 'Search query'),
            ),
            SizedBox(height: 24),
            ElevatedButton(
              onPressed: () {
                context.read(searchQueryProvider).state = _searchQuery;
              },
              child: Text('Search'),
            ),
          ],
        ),
      ),
    );
  }
}

class _RecipesListBody extends StatelessWidget {
  const _RecipesListBody({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer(
      builder: (context, watch, child) {
        final state = watch(recipesListProvider);

        return state.when(
          data: (recipes) {
            return ListView.separated(
              itemBuilder: (context, index) {
                final recipe = recipes[index];

                return ListTile(
                  leading: Icon(Icons.food_bank),
                  title: Text(recipe.title),
                  subtitle: recipe.calories != null
                      ? Text('${recipe.calories} calories')
                      : null,
                  onTap: () => Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (_) => RecipesDetailsPage(recipe: recipe),
                    ),
                  ),
                );
              },
              separatorBuilder: (_, __) => Divider(),
              itemCount: recipes.length,
            );
          },
          loading: () => Center(
            child: CircularProgressIndicator(),
          ),
          error: (_, __) => Center(
            child: Text('Error'),
          ),
        );
      },
    );
  }
}
