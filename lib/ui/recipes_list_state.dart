import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:food_recipes/data/recipes_repository.dart';

final recipesListProvider = FutureProvider<List<RecipeModel>>((ref) {
  final query = ref.watch(searchQueryProvider).state;
  return ref.read(recipesRepositoryProvider).getRecipes(query);
});

final searchQueryProvider = StateProvider((_) => '');
